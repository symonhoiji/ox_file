package oxoop;

import java.io.Serializable;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Symon
 */
public class Player implements Serializable{

    private char name;
    private int win, lose, draw;
    
    public Player(char name){
        this.name = name;
    }
    public char getName() {
        return name;
    }

    public void setName(char name) {
        this.name = name;
    }

    public int getWin() {
        return win;
    }

    public int getLose() {
        return lose;
    }

    public int getDraw() {
        return draw;
    }

    public void win() {
        win++;
    }

    public void lose() {
        lose++;
    }

    public void draw() {
        draw++;
    }
    public void clear(){
        win = 0;
        draw = 0;
        lose = 0;
    } 

    @Override
    public String toString(){
        return "Player { " + "name = " + name + ", win = " + win+ ", lose = "+lose+" }";
    }
}
